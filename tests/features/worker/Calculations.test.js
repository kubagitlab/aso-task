import React from 'react';
import { shallow } from 'enzyme';
import { Calculations } from '../../../src/features/worker/Calculations';

describe('worker/Calculations', () => {
  it('renders node with correct class name', () => {
    const props = {
      worker: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Calculations {...props} />
    );

    expect(
      renderedComponent.find('.worker-calculations').length
    ).toBe(1);
  });
});
