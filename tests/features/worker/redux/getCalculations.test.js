import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  WORKER_GET_CALCULATIONS_BEGIN,
  WORKER_GET_CALCULATIONS_SUCCESS,
  WORKER_GET_CALCULATIONS_FAILURE,
  WORKER_GET_CALCULATIONS_DISMISS_ERROR,
} from '../../../../src/features/worker/redux/constants';

import {
  getCalculations,
  dismissGetCalculationsError,
  reducer,
} from '../../../../src/features/worker/redux/getCalculations';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('worker/redux/getCalculations', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when getCalculations succeeds', () => {
    const store = mockStore({});

    return store.dispatch(getCalculations())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', WORKER_GET_CALCULATIONS_BEGIN);
        expect(actions[1]).toHaveProperty('type', WORKER_GET_CALCULATIONS_SUCCESS);
      });
  });

  it('dispatches failure action when getCalculations fails', () => {
    const store = mockStore({});

    return store.dispatch(getCalculations({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', WORKER_GET_CALCULATIONS_BEGIN);
        expect(actions[1]).toHaveProperty('type', WORKER_GET_CALCULATIONS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissGetCalculationsError', () => {
    const expectedAction = {
      type: WORKER_GET_CALCULATIONS_DISMISS_ERROR,
    };
    expect(dismissGetCalculationsError()).toEqual(expectedAction);
  });

  it('handles action type WORKER_GET_CALCULATIONS_BEGIN correctly', () => {
    const prevState = { getCalculationsPending: false };
    const state = reducer(
      prevState,
      { type: WORKER_GET_CALCULATIONS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getCalculationsPending).toBe(true);
  });

  it('handles action type WORKER_GET_CALCULATIONS_SUCCESS correctly', () => {
    const prevState = { getCalculationsPending: true };
    const state = reducer(
      prevState,
      { type: WORKER_GET_CALCULATIONS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getCalculationsPending).toBe(false);
  });

  it('handles action type WORKER_GET_CALCULATIONS_FAILURE correctly', () => {
    const prevState = { getCalculationsPending: true };
    const state = reducer(
      prevState,
      { type: WORKER_GET_CALCULATIONS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getCalculationsPending).toBe(false);
    expect(state.getCalculationsError).toEqual(expect.anything());
  });

  it('handles action type WORKER_GET_CALCULATIONS_DISMISS_ERROR correctly', () => {
    const prevState = { getCalculationsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: WORKER_GET_CALCULATIONS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getCalculationsError).toBe(null);
  });
});

