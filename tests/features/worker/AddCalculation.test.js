import React from 'react';
import { shallow } from 'enzyme';
import { AddCalculation } from '../../../src/features/worker/AddCalculation';

describe('worker/AddCalculation', () => {
  it('renders node with correct class name', () => {
    const props = {
      worker: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AddCalculation {...props} />
    );

    expect(
      renderedComponent.find('.worker-add-calculation').length
    ).toBe(1);
  });
});
