import React from 'react';
import { shallow } from 'enzyme';
import { AddRequest } from '../../../src/features/admin/AddRequest';

describe('admin/AddRequest', () => {
  it('renders node with correct class name', () => {
    const props = {
      admin: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AddRequest {...props} />
    );

    expect(
      renderedComponent.find('.admin-add-request').length
    ).toBe(1);
  });
});
