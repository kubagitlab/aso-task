import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  ADMIN_GET_REQUESTS_BEGIN,
  ADMIN_GET_REQUESTS_SUCCESS,
  ADMIN_GET_REQUESTS_FAILURE,
  ADMIN_GET_REQUESTS_DISMISS_ERROR,
} from '../../../../src/features/admin/redux/constants';

import {
  getRequests,
  dismissGetRequestsError,
  reducer,
} from '../../../../src/features/admin/redux/getRequests';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('admin/redux/getRequests', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when getRequests succeeds', () => {
    const store = mockStore({});

    return store.dispatch(getRequests())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', ADMIN_GET_REQUESTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', ADMIN_GET_REQUESTS_SUCCESS);
      });
  });

  it('dispatches failure action when getRequests fails', () => {
    const store = mockStore({});

    return store.dispatch(getRequests({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', ADMIN_GET_REQUESTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', ADMIN_GET_REQUESTS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissGetRequestsError', () => {
    const expectedAction = {
      type: ADMIN_GET_REQUESTS_DISMISS_ERROR,
    };
    expect(dismissGetRequestsError()).toEqual(expectedAction);
  });

  it('handles action type ADMIN_GET_REQUESTS_BEGIN correctly', () => {
    const prevState = { getRequestsPending: false };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_REQUESTS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getRequestsPending).toBe(true);
  });

  it('handles action type ADMIN_GET_REQUESTS_SUCCESS correctly', () => {
    const prevState = { getRequestsPending: true };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_REQUESTS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getRequestsPending).toBe(false);
  });

  it('handles action type ADMIN_GET_REQUESTS_FAILURE correctly', () => {
    const prevState = { getRequestsPending: true };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_REQUESTS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getRequestsPending).toBe(false);
    expect(state.getRequestsError).toEqual(expect.anything());
  });

  it('handles action type ADMIN_GET_REQUESTS_DISMISS_ERROR correctly', () => {
    const prevState = { getRequestsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_REQUESTS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getRequestsError).toBe(null);
  });
});

