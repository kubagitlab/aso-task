import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  ADMIN_GET_WORKERS_BEGIN,
  ADMIN_GET_WORKERS_SUCCESS,
  ADMIN_GET_WORKERS_FAILURE,
  ADMIN_GET_WORKERS_DISMISS_ERROR,
} from '../../../../src/features/admin/redux/constants';

import {
  getWorkers,
  dismissGetWorkersError,
  reducer,
} from '../../../../src/features/admin/redux/getWorkers';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('admin/redux/getWorkers', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when getWorkers succeeds', () => {
    const store = mockStore({});

    return store.dispatch(getWorkers())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', ADMIN_GET_WORKERS_BEGIN);
        expect(actions[1]).toHaveProperty('type', ADMIN_GET_WORKERS_SUCCESS);
      });
  });

  it('dispatches failure action when getWorkers fails', () => {
    const store = mockStore({});

    return store.dispatch(getWorkers({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', ADMIN_GET_WORKERS_BEGIN);
        expect(actions[1]).toHaveProperty('type', ADMIN_GET_WORKERS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissGetWorkersError', () => {
    const expectedAction = {
      type: ADMIN_GET_WORKERS_DISMISS_ERROR,
    };
    expect(dismissGetWorkersError()).toEqual(expectedAction);
  });

  it('handles action type ADMIN_GET_WORKERS_BEGIN correctly', () => {
    const prevState = { getWorkersPending: false };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_WORKERS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getWorkersPending).toBe(true);
  });

  it('handles action type ADMIN_GET_WORKERS_SUCCESS correctly', () => {
    const prevState = { getWorkersPending: true };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_WORKERS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getWorkersPending).toBe(false);
  });

  it('handles action type ADMIN_GET_WORKERS_FAILURE correctly', () => {
    const prevState = { getWorkersPending: true };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_WORKERS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getWorkersPending).toBe(false);
    expect(state.getWorkersError).toEqual(expect.anything());
  });

  it('handles action type ADMIN_GET_WORKERS_DISMISS_ERROR correctly', () => {
    const prevState = { getWorkersError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: ADMIN_GET_WORKERS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getWorkersError).toBe(null);
  });
});

