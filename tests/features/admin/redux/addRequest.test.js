import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  ADMIN_ADD_REQUEST_BEGIN,
  ADMIN_ADD_REQUEST_SUCCESS,
  ADMIN_ADD_REQUEST_FAILURE,
  ADMIN_ADD_REQUEST_DISMISS_ERROR,
} from '../../../../src/features/admin/redux/constants';

import {
  addRequest,
  dismissAddRequestError,
  reducer,
} from '../../../../src/features/admin/redux/addRequest';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('admin/redux/addRequest', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when addRequest succeeds', () => {
    const store = mockStore({});

    return store.dispatch(addRequest())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', ADMIN_ADD_REQUEST_BEGIN);
        expect(actions[1]).toHaveProperty('type', ADMIN_ADD_REQUEST_SUCCESS);
      });
  });

  it('dispatches failure action when addRequest fails', () => {
    const store = mockStore({});

    return store.dispatch(addRequest({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', ADMIN_ADD_REQUEST_BEGIN);
        expect(actions[1]).toHaveProperty('type', ADMIN_ADD_REQUEST_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissAddRequestError', () => {
    const expectedAction = {
      type: ADMIN_ADD_REQUEST_DISMISS_ERROR,
    };
    expect(dismissAddRequestError()).toEqual(expectedAction);
  });

  it('handles action type ADMIN_ADD_REQUEST_BEGIN correctly', () => {
    const prevState = { addRequestPending: false };
    const state = reducer(
      prevState,
      { type: ADMIN_ADD_REQUEST_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addRequestPending).toBe(true);
  });

  it('handles action type ADMIN_ADD_REQUEST_SUCCESS correctly', () => {
    const prevState = { addRequestPending: true };
    const state = reducer(
      prevState,
      { type: ADMIN_ADD_REQUEST_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addRequestPending).toBe(false);
  });

  it('handles action type ADMIN_ADD_REQUEST_FAILURE correctly', () => {
    const prevState = { addRequestPending: true };
    const state = reducer(
      prevState,
      { type: ADMIN_ADD_REQUEST_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addRequestPending).toBe(false);
    expect(state.addRequestError).toEqual(expect.anything());
  });

  it('handles action type ADMIN_ADD_REQUEST_DISMISS_ERROR correctly', () => {
    const prevState = { addRequestError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: ADMIN_ADD_REQUEST_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addRequestError).toBe(null);
  });
});

