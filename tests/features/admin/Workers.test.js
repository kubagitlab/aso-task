import React from 'react';
import { shallow } from 'enzyme';
import { Workers } from '../../../src/features/admin/Workers';

describe('admin/Workers', () => {
  it('renders node with correct class name', () => {
    const props = {
      admin: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Workers {...props} />
    );

    expect(
      renderedComponent.find('.admin-workers').length
    ).toBe(1);
  });
});
