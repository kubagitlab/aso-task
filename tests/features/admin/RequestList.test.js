import React from 'react';
import { shallow } from 'enzyme';
import { RequestList } from '../../../src/features/admin/RequestList';

describe('admin/RequestList', () => {
  it('renders node with correct class name', () => {
    const props = {
      admin: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <RequestList {...props} />
    );

    expect(
      renderedComponent.find('.admin-request-list').length
    ).toBe(1);
  });
});
