import React from 'react';
import { shallow } from 'enzyme';
import { RequestDetail } from '../../../src/features/admin/RequestDetail';

describe('admin/RequestDetail', () => {
  it('renders node with correct class name', () => {
    const props = {
      admin: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <RequestDetail {...props} />
    );

    expect(
      renderedComponent.find('.admin-request-detail').length
    ).toBe(1);
  });
});
