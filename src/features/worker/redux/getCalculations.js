import axios from 'axios';
import { useEffect, useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {
  WORKER_GET_CALCULATIONS_BEGIN,
  WORKER_GET_CALCULATIONS_SUCCESS,
  WORKER_GET_CALCULATIONS_FAILURE,
  WORKER_GET_CALCULATIONS_DISMISS_ERROR,
} from './constants';

export function getCalculations(args = {}) {
  return dispatch => {
    // optionally you can have getState as the second argument
    dispatch({
      type: WORKER_GET_CALCULATIONS_BEGIN,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.get('http://127.0.0.1:8000/calculations/');
      doRequest.then(
        res => {
          dispatch({
            type: WORKER_GET_CALCULATIONS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        err => {
          dispatch({
            type: WORKER_GET_CALCULATIONS_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function dismissGetCalculationsError() {
  return {
    type: WORKER_GET_CALCULATIONS_DISMISS_ERROR,
  };
}

export function useGetCalculations() {
  const dispatch = useDispatch();

  const { getCalculationsPending, getCalculationsError } = useSelector(
    state => ({
      getCalculationsPending: state.worker.getCalculationsPending,
      getCalculationsError: state.worker.getCalculationsError,
    }),
    shallowEqual,
  );

  const boundAction = useCallback(
    (...args) => {
      return dispatch(getCalculations(...args));
    },
    [dispatch],
  );

  const boundDismissError = useCallback(() => {
    return dispatch(dismissGetCalculationsError());
  }, [dispatch]);

  return {
    getCalculations: boundAction,
    getCalculationsPending,
    getCalculationsError,
    dismissGetCalculationsError: boundDismissError,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case WORKER_GET_CALCULATIONS_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        getCalculationsPending: true,
        getCalculationsError: null,
      };

    case WORKER_GET_CALCULATIONS_SUCCESS:
      // The request is success
      return {
        ...state,
        getCalculationsPending: false,
        getCalculationsError: null,
        calculationList: action.data,
      };

    case WORKER_GET_CALCULATIONS_FAILURE:
      // The request is failed
      return {
        ...state,
        getCalculationsPending: false,
        getCalculationsError: action.data.error,
      };

    case WORKER_GET_CALCULATIONS_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        getCalculationsError: null,
      };

    default:
      return state;
  }
}
