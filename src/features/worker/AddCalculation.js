import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

export class AddCalculation extends Component {
  static propTypes = {
    worker: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="worker-add-calculation">
        Page Content: worker/AddCalculation
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    worker: state.worker,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddCalculation);
