import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

export class Calculations extends Component {
  static propTypes = {
    worker: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="worker-calculations">
        <button onClick={this.props.actions.getCalculations}>Расчеты</button>
        <br/>
        <br/>
        {this.props.worker.calculationList && (
          <ul>
            {this.props.worker.calculationList.map(item => (
              <li key={item.id}><a href={'/worker/calculation/add'}> {item.price} </a></li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    worker: state.worker,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Calculations);
