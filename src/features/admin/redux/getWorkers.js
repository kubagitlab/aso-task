import axios from 'axios';
import { useEffect, useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {
  ADMIN_GET_WORKERS_BEGIN,
  ADMIN_GET_WORKERS_SUCCESS,
  ADMIN_GET_WORKERS_FAILURE,
  ADMIN_GET_WORKERS_DISMISS_ERROR,
} from './constants';

export function getWorkers(args = {}) {
  return dispatch => {
    // optionally you can have getState as the second argument
    dispatch({
      type: ADMIN_GET_WORKERS_BEGIN,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.get('http://127.0.0.1:8000/performers/');
      doRequest.then(
        res => {
          console.log('respp:', res);
          dispatch({
            type: ADMIN_GET_WORKERS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        err => {
          dispatch({
            type: ADMIN_GET_WORKERS_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function dismissGetWorkersError() {
  return {
    type: ADMIN_GET_WORKERS_DISMISS_ERROR,
  };
}

export function useGetWorkers() {
  const dispatch = useDispatch();

  const { getWorkersPending, getWorkersError } = useSelector(
    state => ({
      getWorkersPending: state.admin.getWorkersPending,
      getWorkersError: state.admin.getWorkersError,
    }),
    shallowEqual,
  );

  const boundAction = useCallback(
    (...args) => {
      return dispatch(getWorkers(...args));
    },
    [dispatch],
  );

  const boundDismissError = useCallback(() => {
    return dispatch(dismissGetWorkersError());
  }, [dispatch]);

  return {
    getWorkers: boundAction,
    getWorkersPending,
    getWorkersError,
    dismissGetWorkersError: boundDismissError,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case ADMIN_GET_WORKERS_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        getWorkersPending: true,
        getWorkersError: null,
      };

    case ADMIN_GET_WORKERS_SUCCESS:
      // The request is success
      return {
        ...state,
        getWorkersPending: false,
        getWorkersError: null,
        workers: action.data,
      };

    case ADMIN_GET_WORKERS_FAILURE:
      // The request is failed
      return {
        ...state,
        getWorkersPending: false,
        getWorkersError: action.data.error,
      };

    case ADMIN_GET_WORKERS_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        getWorkersError: null,
      };

    default:
      return state;
  }
}
