import axios from 'axios';
import { useEffect, useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {
  ADMIN_ADD_REQUEST_BEGIN,
  ADMIN_ADD_REQUEST_SUCCESS,
  ADMIN_ADD_REQUEST_FAILURE,
  ADMIN_ADD_REQUEST_DISMISS_ERROR,
} from './constants';

export function addRequest(args = {}) {
  return dispatch => {
    // optionally you can have getState as the second argument
    dispatch({
      type: ADMIN_ADD_REQUEST_BEGIN,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.post('http://127.0.0.1:8000/requests/');
      doRequest.then(
        res => {
          dispatch({
            type: ADMIN_ADD_REQUEST_SUCCESS,
            data: res,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        err => {
          dispatch({
            type: ADMIN_ADD_REQUEST_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function dismissAddRequestError() {
  return {
    type: ADMIN_ADD_REQUEST_DISMISS_ERROR,
  };
}

export function useAddRequest() {
  const dispatch = useDispatch();

  const { addRequestPending, addRequestError } = useSelector(
    state => ({
      addRequestPending: state.admin.addRequestPending,
      addRequestError: state.admin.addRequestError,
    }),
    shallowEqual,
  );

  const boundAction = useCallback(
    (...args) => {
      return dispatch(addRequest(...args));
    },
    [dispatch],
  );

  const boundDismissError = useCallback(() => {
    return dispatch(dismissAddRequestError());
  }, [dispatch]);

  return {
    addRequest: boundAction,
    addRequestPending,
    addRequestError,
    dismissAddRequestError: boundDismissError,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case ADMIN_ADD_REQUEST_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        addRequestPending: true,
        addRequestError: null,
      };

    case ADMIN_ADD_REQUEST_SUCCESS:
      // The request is success
      return {
        ...state,
        addRequestPending: false,
        addRequestError: null,
      };

    case ADMIN_ADD_REQUEST_FAILURE:
      // The request is failed
      return {
        ...state,
        addRequestPending: false,
        addRequestError: action.data.error,
      };

    case ADMIN_ADD_REQUEST_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        addRequestError: null,
      };

    default:
      return state;
  }
}
