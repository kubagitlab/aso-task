import axios from 'axios';
import { useEffect, useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {
  ADMIN_GET_REQUESTS_BEGIN,
  ADMIN_GET_REQUESTS_SUCCESS,
  ADMIN_GET_REQUESTS_FAILURE,
  ADMIN_GET_REQUESTS_DISMISS_ERROR,
} from './constants';

export function getRequests(args = {}) {
  return dispatch => {
    // optionally you can have getState as the second argument
    dispatch({
      type: ADMIN_GET_REQUESTS_BEGIN,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.get('http://127.0.0.1:8000/requests/');
      doRequest.then(
        res => {
          dispatch({
            type: ADMIN_GET_REQUESTS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        err => {
          dispatch({
            type: ADMIN_GET_REQUESTS_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function dismissGetRequestsError() {
  return {
    type: ADMIN_GET_REQUESTS_DISMISS_ERROR,
  };
}

export function useGetRequests() {
  const dispatch = useDispatch();

  const { getRequestsPending, getRequestsError } = useSelector(
    state => ({
      getRequestsPending: state.admin.getRequestsPending,
      getRequestsError: state.admin.getRequestsError,
    }),
    shallowEqual,
  );

  const boundAction = useCallback(
    (...args) => {
      return dispatch(getRequests(...args));
    },
    [dispatch],
  );

  const boundDismissError = useCallback(() => {
    return dispatch(dismissGetRequestsError());
  }, [dispatch]);

  return {
    getRequests: boundAction,
    getRequestsPending,
    getRequestsError,
    dismissGetRequestsError: boundDismissError,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case ADMIN_GET_REQUESTS_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        getRequestsPending: true,
        getRequestsError: null,
      };

    case ADMIN_GET_REQUESTS_SUCCESS:
      // The request is success
      return {
        ...state,
        getRequestsPending: false,
        getRequestsError: null,
        requestList: action.data,
      };

    case ADMIN_GET_REQUESTS_FAILURE:
      // The request is failed
      return {
        ...state,
        getRequestsPending: false,
        getRequestsError: action.data.error,
      };

    case ADMIN_GET_REQUESTS_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        getRequestsError: null,
      };

    default:
      return state;
  }
}
