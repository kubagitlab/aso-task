import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';

export class RequestList extends Component {
  static propTypes = {
    admin: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="admin-request-list">
        <button onClick={this.props.actions.getRequests}>Запросы</button>
        <a href={'/admin/request/add'}> Добавить </a>
        <br/>
        <br/>
        {this.props.admin.requestList && (
          <ul>
            {this.props.admin.requestList.map(item => (
              <li key={item.id}><a href={'/admin/request/detail'}> {item.name} </a></li>
            ))}
          </ul>
        )}
        <br/>
        <br/>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    admin: state.admin,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RequestList);
