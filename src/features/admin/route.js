// This is the JSON way to define React Router rules in a Rekit app.
// Learn more from: http://rekit.js.org/docs/routing.html

import { Workers, RequestList, AddRequest, RequestDetail } from './';

export default {
  path: 'admin',
  childRoutes: [
    { path: 'workers', component: Workers },
    { path: 'requests', component: RequestList },
    { path: 'request/add', component: AddRequest },
    { path: 'request/detail', component: RequestDetail },
  ],
};
